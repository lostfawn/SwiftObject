//
//  ATPickerPhoto.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/9/10.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ATPickerPhoto: NSObject {
    static let manager = ATPickerPhoto()
    private var completion :((_ image :UIImage?) -> Void)!
    private var completions :((_ listData :[UIImage]) -> Void)!
    public class func pickerPhoto(completion :@escaping ((_ image : UIImage?) -> Void)){
        ATActionSheet.showActionSheet(title:"请选择",normals: ["拍照","相册选择"], hights:["取消"]) { (title , index) in
            if index == 0{
                pickerPhoto(sourceType: .camera, completion: completion)
            }else if index == 1{
                pickerPhoto(sourceType: .photoLibrary, completion: completion)
            }
        }
    }
    public class func pickerPhoto(sourceType :UIImagePickerController.SourceType,completion :@escaping ((_ image : UIImage?) -> Void)){
        let auth = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if auth == .denied {
            ATAlertView.showAlertView(title:"App相机权限受限", message: "请在设置中允许访问相机", normals:["取消"], hights: ["确定"]) { (title , index) in
                if index == 0{
                    return
                }
                if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!){
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                }
            }
            return
        }
        ATPickerPhoto.manager.completion = completion
        let vc = UIViewController.rootTopPresentedController()
        let picker = UIImagePickerController()
        picker.sourceType = sourceType
        picker.delegate = ATPickerPhoto.manager
        picker.allowsEditing = true
        picker.modalPresentationStyle = .fullScreen
        vc.present(picker, animated: true, completion: nil)
    } 
//    public class func pickerPhoto(count :Int,completion :@escaping ((_ listData : [UIImage]) -> Void)){
//        
//    }
}
extension ATPickerPhoto : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        ATPickerPhoto.manager.completion(nil)
        UIViewController.rootTopPresentedController().dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image :UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if image.isKind(of:UIImage.self) {
            ATPickerPhoto.manager.completion(image)
        }else{
            ATPickerPhoto.manager.completion(nil)
        }
        UIViewController.rootTopPresentedController().dismiss(animated: true, completion: nil)
    }
}
