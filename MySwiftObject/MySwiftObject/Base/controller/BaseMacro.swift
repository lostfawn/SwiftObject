//
//  BaseMacro.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/30.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import ATKit_Swift
import Hue
import SnapKit
import SwiftyJSON

let SCREEN_WIDTH  :CGFloat  = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT :CGFloat  = UIScreen.main.bounds.size.height

let iPhone_X        : Bool      = ATMacro.iPhoneX()
let STATUS_BAR_HIGHT:CGFloat    = iPhone_X ? 44 : 20  //状态栏
let NAVI_BAR_HIGHT  :CGFloat    = iPhone_X ? 88 : 64  //导航栏
let TAB_BAR_ADDING  :CGFloat    = iPhone_X ? 32 : 0  //iphoneX斜刘海

let AppColor     :UIColor = UIColor(hex:"007EFE")
let Appxdddddd   :UIColor = UIColor(hex:BaseMacro.Axdddddd())
let Appx000000   :UIColor = UIColor(hex:BaseMacro.Ax000000())
let Appx181818   :UIColor = UIColor(hex:BaseMacro.Ax181818())
let Appx333333   :UIColor = UIColor(hex:BaseMacro.Ax333333())
let Appx666666   :UIColor = UIColor(hex:BaseMacro.Ax666666())
let Appx999999   :UIColor = UIColor(hex:BaseMacro.Ax999999())
let Appxf8f8f8   :UIColor = UIColor(hex:BaseMacro.Axf8f8f8())
let Appxffffff   :UIColor = UIColor(hex:BaseMacro.Axffffff())
let AppRadius    :CGFloat = 3
let placeholder  :UIImage = UIImage.imageWithColor(color:UIColor(hex: "f4f4f4"))

let appDatas : [String] = ["七届传说","极品家丁","择天记","神墓","遮天"]

let AppFrame :CGRect = CGRect.init(x:20, y:STATUS_BAR_HIGHT+20, width: SCREEN_WIDTH - 40, height:CGFloat(SCREEN_HEIGHT-STATUS_BAR_HIGHT-20-20-TAB_BAR_ADDING));

class BaseMacro : NSObject{
    class func Axffffff() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "ffffff" : "181818"
    }
    class func Axdddddd() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "dddddd" : "ededed"
    }
    class func Ax000000() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "000000" : "ffffff"
    }
    class func Ax181818() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "181818" : "ffffff"
    }
    class func Ax333333() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "333333" : "ffffff"
    }
    class func Ax666666() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "666666" : "ffffff"
    }
    class func Ax999999() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "999999" : "999999"
    }
    class func Axf8f8f8() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "f8f8f8" : "181818"
    }
    class func Axf4f4f4() -> String{
        let theme = GKThemeTool.manager.theme
        return theme?.state == .Day ? "f4f4f4" : "252631"
    }
}
