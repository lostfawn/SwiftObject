//
//  AppDelegate.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/12/3.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.window?.backgroundColor = UIColor.white
        let vc = GKLaunchController {
            GKJump.jumpToGuideCtrl {
                self.window?.rootViewController = GKTabBarController()
            }
        }
        self.window?.rootViewController = vc
        
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
//        UIApplication.shared.beginBackgroundTask {
//
//        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
//        let path : URL = URL.init(string: "https://api.zhuishushenqi.com/cats/lv2/statistics")!
//        let conifg : URLSessionConfiguration = URLSessionConfiguration.default;
//        let session : URLSession = URLSession.init(configuration: conifg);
//        var request = URLRequest.init(url: path)
//        request.timeoutInterval = 10;
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
//        request.addValue("chrome", forHTTPHeaderField:"User-Agent")
//        let task : URLSessionDataTask = session.dataTask(with: request) { (data, respond, error) in
//            let json = JSON(data as Any)
//            print(json)
//        }
//        task.resume();
//        sleep(2)
    }


}

