//
//  GKNovelTableViewCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/9/8.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit

class GKNovelTableViewCell: UITableViewCell {

    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var contentLab: UILabel!
    var content :NSAttributedString?{
        didSet{
            guard let content = content else { return }
           // let model :GKNovelSet = GKNovelSetManager.manager.config!
            self.contentLab.attributedText = content
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
