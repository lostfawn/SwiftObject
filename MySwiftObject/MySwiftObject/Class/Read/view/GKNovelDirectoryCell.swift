//
//  GKNovelDirectoryCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/19.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKNovelDirectoryCell: UITableViewCell {
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageLock: UIImageView!
    
//    var _select : Bool = false;
//    var select : Bool{
//        set{
//            _select = newValue;
//            self.titleLab.textColor = _select ? AppColor : Appx333333;
//        }get{
//            return _select;
//        }
//    }
    var  select :Bool?{
        didSet{
            guard let select = select else { return }
            self.titleLab.textColor = select ? AppColor : Appx333333;
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.tag = 10086;
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
