//
//  GKNovelSet.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON

public enum GKNovelTheme : String,HandyJSONEnum{
    case defaults = "icon_default"
    case green    = "icon_read_green"
    case caffee   = "icon_read_coffee"
    case pink     = "icon_read_fenweak"
    case fen      = "icon_read_fen"
    case zi       = "icon_read_zi"
    case yellow   = "icon_read_yellow"
    case phone    = "icon_phone"
    case hite     = "icon_hite"
}
public enum GKNovelBrowse : Int,HandyJSONEnum{
    case defaults = 0//默认
    case pageCurl = 1//翻页
    case none     = 2//无动画
    case scroll   = 3//上下滑动
}
public class GKNovelSet: HandyJSON {
    var color:String        = "333333"
    var fontName:String     = "PingFang-SC-Regular"
    var font : Float        = 18
    var lineSpacing :Float  = 5
    var firstLineHeadIndent : Float   = 20
    var paragraphSpacingBefore :Float = 5
    var paragraphSpacing : Float      = 5
    var brightness :Float             = 0
    
    var night :Bool                   = false
    var landscape :Bool               = false
    var traditiona :Bool              = false
    
    var skin   :GKNovelTheme          = .defaults
    var browse :GKNovelBrowse         = .defaults
    required public init() {}
}

