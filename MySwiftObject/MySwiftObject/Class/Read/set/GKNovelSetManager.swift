//
//  GKNovelSetManager.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON

public  let config = GKNovelSetManager.set()
private let GKSetInfo :String = "GKSetTheme";
class GKNovelSetManager: NSObject {
    
    class func set() -> GKNovelSet{
        let defaults :UserDefaults = UserDefaults.standard
        let data = defaults.object(forKey:GKSetInfo)
        let json = JSON(data as Any)
        if let model : GKNovelSet = GKNovelSet.deserialize(from: json.rawString()){
            return model
        }
        return GKNovelSet()
    }
    class func setNight(night:Bool){
        if config.night != night {
            config.night = night
            GKNovelSetManager.saveConfig(config: config)
        }
    }
    class func setLandscape(landscape:Bool){
        if config.landscape != landscape {
            config.landscape = landscape
            GKNovelSetManager.saveConfig(config: config)
        }
    }
    class func setTraditiona(traditiona:Bool){
        let model : GKNovelSet = config
        if model.traditiona != traditiona {
            model.traditiona = traditiona
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func setBrightness(brightness:Float){
        let model : GKNovelSet = config
        if model.brightness != brightness {
            model.brightness = brightness
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func setFontName(fontName:String){
        let model : GKNovelSet = config
        if model.fontName != fontName {
            model.fontName = fontName
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func setFont(font:Float){
        let model : GKNovelSet = config
        if model.font != font {
            model.font = font
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func setSkin(skin:GKNovelTheme){
        let model : GKNovelSet = config
        if model.skin != skin {
            model.skin = skin
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func setBrowse(browse:GKNovelBrowse){
        let model : GKNovelSet = config
        if model.browse != browse {
            model.browse = browse
            GKNovelSetManager.saveConfig(config: model)
        }
    }
    class func saveConfig(config :GKNovelSet){
        let defaults :UserDefaults = UserDefaults.standard
        if let data = config.toJSON(){
            defaults.set(data, forKey:GKSetInfo)
            defaults.synchronize()
        }
    }
    class func defaultFont()-> [NSAttributedString.Key: Any]{
        let model : GKNovelSet = config
        let paragraphStyle : NSMutableParagraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineSpacing = CGFloat(model.lineSpacing )//段落 行间距
        paragraphStyle.firstLineHeadIndent  = CGFloat(model.firstLineHeadIndent )//首行缩进
        paragraphStyle.paragraphSpacingBefore = CGFloat(model.paragraphSpacingBefore ) //段间距，当前段落和上个段落之间的距离。
        paragraphStyle.paragraphSpacing = CGFloat(model.paragraphSpacing ) //段间距，当前段落和下个段落之间的距离。
        paragraphStyle.alignment = .justified//两边对齐
        paragraphStyle.allowsDefaultTighteningForTruncation = true
        var att :[NSAttributedString.Key : Any] = [:]
        let font : UIFont = UIFont.init(name: model.fontName, size:CGFloat(model.font))!
        let color : UIColor = (model.skin == .caffee) ? Appxdddddd : UIColor.init(hex:model.color )
        att.updateValue(font, forKey: .font)
        att.updateValue(color, forKey: .foregroundColor)
        att.updateValue(paragraphStyle, forKey: .paragraphStyle)
        return att
    }
    class func defaultSkin() ->UIImage{
        let model : GKNovelSet = config
        if model.night {
            return UIImage.init(named: "icon_read_black")!
        }
        return UIImage.init(named: model.skin.rawValue)!
    }
    class func themes() -> [String]{
        return [GKNovelTheme.defaults.rawValue,GKNovelTheme.green.rawValue,GKNovelTheme.caffee.rawValue,GKNovelTheme.pink.rawValue,GKNovelTheme.fen.rawValue,GKNovelTheme.zi.rawValue,GKNovelTheme.yellow.rawValue,GKNovelTheme.phone.rawValue,GKNovelTheme.hite.rawValue];
    }
}

