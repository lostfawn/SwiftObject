//
//  GKMineTableViewCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKMineTableViewCell: UITableViewCell {

    @IBOutlet weak var imageRight: UIImageView!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lineView: UIView!
    var model : GKMineModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title
            self.imageV.image = UIImage(named: item.icon)
            let theme = GKThemeTool.manager.theme
            self.switchBtn.isOn = theme?.state == GKThemeState.Night
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func changeAction(_ sender: UISwitch) {
        let theme = GKThemeTool.manager.theme
        if theme?.state == .Day {
            GKThemeTool.setState(state:GKThemeState.Night)
        }else{
            GKThemeTool.setState(state:GKThemeState.Day)
        }
    }
    
}
