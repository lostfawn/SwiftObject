//
//  GKMineController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKMineController: BaseTableViewController {
    private let bookCase   :String = "我的书架"
    private let bookBrowse :String = "浏览记录"
    private let about      :String = "夜间模式"
    
    private lazy var listData: [GKMineModel] = {
        return [];
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupEmpty(scrollView: self.tableView)
        self.setupRefresh(scrollView: self.tableView, options:.defaults)
    }
    override func refreshData(page: Int) {
        let model1:GKMineModel = GKMineModel(title:bookCase, icon: "icon_fav", subTitle:"")
        let model2:GKMineModel = GKMineModel(title:bookBrowse, icon: "icon_historys", subTitle:"")
        let model3:GKMineModel = GKMineModel(title:about, icon: "icon_option", subTitle:"")
        self.listData = [model1,model2,model3]
        self.tableView.reloadData()
        self.endRefreshFailure();
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count;
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GKMineTableViewCell = GKMineTableViewCell.cellForTableView(tableView: tableView, indexPath: indexPath) 
        let model :GKMineModel = self.listData[indexPath.row];
        cell.model = model
        cell.lineView.isHidden = indexPath.row + 1 == self.listData.count;
        cell.imageRight.isHidden = (model.title == about)
        return cell;
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        let model :GKMineModel = self.listData[indexPath.row];
        if model.title == bookCase {
            GKJump.jumpToBookCase();
        }else if model.title == bookBrowse{
            GKJump.jumpToBrowse();
        }else{
            
        }
    }
}
