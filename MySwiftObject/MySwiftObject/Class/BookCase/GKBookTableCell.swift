//
//  GKBookTableCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/23.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBookTableCell: UICollectionViewCell {

    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var countLab: UILabel!
    @IBOutlet weak var contentLab: UILabel!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var imageV: UIImageView!
    
    @IBOutlet weak var nickNameBtn: UIButton!
    func attTitle(content :String) -> NSAttributedString {
        let paragraphStyle  = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .left
        paragraphStyle.allowsDefaultTighteningForTruncation = true
        let att = NSAttributedString.init(string:content.count > 0 ? content : "这家伙很懒,暂无简介!", attributes:[NSAttributedString.Key.paragraphStyle : paragraphStyle])
        return att
    }
    var model:GKBookModel?{
        didSet{
            guard let item = model else { return }
            self.imageV.setGkImageWithURL(url: item.cover  ?? "");
            self.titleLab.text = item.title ?? "";
            self.contentLab.attributedText = attTitle(content: item.shortIntro ?? "")
            self.countLab.text = GKNumber.getCount(count: item.latelyFollower ?? 0);
            self.stateBtn.setTitle(item.majorCate ?? "", for: .normal);
            self.stateBtn.isHidden = (item.majorCate!.count > 0) ? false : true;
            self.favBtn.setTitle("关注:"+String(item.retentionRatio)+("%"), for: .normal);
            
            self.nickNameBtn .setTitle(item.author ?? "", for: .normal)
            self.nickNameBtn.isHidden = item.author!.count > 0 ? false : true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.stateBtn.layer.masksToBounds = true;
        self.stateBtn.layer.cornerRadius = 7.5;
        self.favBtn.layer.masksToBounds = true;
        self.favBtn.layer.cornerRadius = 10;
        
        self.imageV.layer.masksToBounds = true;
        self.imageV.layer.cornerRadius = AppRadius

        self.nickNameBtn.layer.masksToBounds = true
          self.nickNameBtn.layer.cornerRadius = AppRadius
          self.nickNameBtn.backgroundColor = Appxf8f8f8
          self.nickNameBtn.setTitleColor(Appx666666, for: .normal)
        
        self.favBtn.setTitleColor(AppColor, for: .normal)
        
        // Initialization code
    }

}
