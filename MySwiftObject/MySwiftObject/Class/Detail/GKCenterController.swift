//
//  GKCenterController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/3/31.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON
import ATRefresh_Swift
class GKCenterController: BaseConnectionController {
    convenience init(bookId :String, model :GKBookDetailModel? = nil) {
        self.init()
        self.bookId = bookId
        self.model = model
    }
    private lazy var listData: [GKBookModel] = {
        return []
    }()
    private lazy var titleLab: UILabel = {
        let label : UILabel = UILabel.init();
        label.font = UIFont.systemFont(ofSize:12)
        label.numberOfLines = 2;
        label.textColor = Appx666666;
        return label
    }()
    public var model  : GKBookDetailModel?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    private var bookId : String? = "";
    private let top :CGFloat = 15;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupEmpty(scrollView: self.collectionView);
        self.setupRefresh(scrollView: self.collectionView, options: ATRefreshOption(rawValue: ATRefreshOption.header.rawValue | ATRefreshOption.autoHeader.rawValue));
        self.collectionView.backgroundColor = Appxffffff
        self.collectionView.backgroundView?.backgroundColor = Appxffffff
    }
    override func refreshData(page: Int) {
        if bookId!.count > 0 {
            GKClassifyNet.bookCommend(bookId: self.bookId!, sucesss: { (object) in
                if let data = [GKBookModel].deserialize(from: object["books"].rawString()){
                    self.listData = data as! [GKBookModel];
                }
                self.collectionView.reloadData();
                self.endRefresh(more: false)
            }) { (error) in
                self.endRefreshFailure();
            };
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1;
        }
        override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.listData.count;
        }
        override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return self.top;
        }
        override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return self.top;
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize.init(width: SCREEN_WIDTH, height: self.model != nil ? GKDetailReusableView.getHeight(model: self.model!) : 0.001)
        }
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            let view = GKDetailReusableView.viewForCollectionView(collectionView: collectionView, elementKind: kind, indexPath: indexPath)
            view.isHidden = self.model == nil
            if self.model != nil {
                view.contentLab.numberOfLines = self.model!.numberLine
                view.content = self.model?.shortIntro
                view.moreBtn.isSelected = self.model?.numberLine == 0
            }
            view.moreBtn.addTarget(self, action: #selector(moreAction(sender:)), for: .touchUpInside)
            return view
        }
        override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: self.top, left: self.top, bottom: self.top, right: self.top);
        }
        override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = (SCREEN_WIDTH - self.top * 4)/3.0 - 0.1;
            let height = width * 1.35 + 50;
            return CGSize.init(width: width, height: height);
        }
        override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell :GKBookCell = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
            cell.model = self.listData[indexPath.row] ;
            return cell;
        }
        override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let model:GKBookModel = self.listData[indexPath.row] ;
            GKJump.jumpToDetail(bookId: model.bookId!);
        }
        @objc func moreAction(sender : UIButton){
            self.model?.numberLine = self.model?.numberLine == 3 ? 0 : 3
        
            self.collectionView.reloadData()
        }
        override func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
            return 320/2;
        }
}
