//
//  GKSearchResultController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/20.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON

class GKSearchResultController: BaseTableViewController {
    convenience init(keyWord : String) {
        self.init()
        self.keyWord = keyWord
    }
    private var keyWord : String?{
        didSet{
            refreshData(page: 1)
        }
    }
    private lazy var listData : [GKBookModel] = {
        return []
    }()
    private lazy var searchView : GKSearchTextView = {
        let search = GKSearchTextView.instanceView()
        search.delegate = self
        return search
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fd_prefersNavigationBarHidden = true
        loadUI()
    }
    private func loadUI(){
        self.view.addSubview(self.searchView)
        self.searchView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(NAVI_BAR_HIGHT)
        }
        self.tableView.snp.remakeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(self.searchView.snp.bottom)
        }
        self.setupEmpty(scrollView: self.tableView)
        self.setupRefresh(scrollView: self.tableView, options:.defaults)
        self.searchView.keyword = self.keyWord
    }
    override func refreshData(page: Int) {
        if self.keyWord?.count == 0 {
            return
        }
        GKClassifyNet.bookSearch(hotWord: self.keyWord!, page: page, size: RefreshPageSize, sucesss: { (object) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if page == 1{
                self.listData.removeAll();
            }
            if let datas = [GKBookModel].deserialize(from: object["books"].rawString()){
                let list = datas as! [GKBookModel];
                if list.count > 0 {
                    self.listData.append(contentsOf: list);
                }
                self.tableView.reloadData();
                self.endRefresh(more: datas.count >= RefreshPageSize);
            }else{
                self.endRefreshFailure();
            }
            
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.endRefreshFailure();
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count;
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GKClassifyTailCell = GKClassifyTailCell.cellForTableView(tableView: tableView, indexPath: indexPath);
        cell.model = self.listData[indexPath.row];
        return cell;
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model :GKBookModel = self.listData[indexPath.row];
        GKJump.jumpToDetail(bookId: model.bookId!)
    }
}
extension GKSearchResultController : GKSearchDelegate{

    func searchTopView(topView: GKSearchTextView, goback: Bool) {
        self.goBack()
    }
    func searchTopView(topView: GKSearchTextView, keyword: String) {
        self.keyWord = keyword
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
}
