//
//  GKSearchTextView.swift
//  ATSeek
//
//  Created by wangws1990 on 2018/6/22.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
@objc protocol GKSearchDelegate :NSObjectProtocol{
     @objc optional func searchTopView(topView:GKSearchTextView,keyword:String)
     @objc optional func searchTopView(topView:GKSearchTextView,goback :Bool)
}
class GKSearchTextView: UIView,UITextFieldDelegate {
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    weak var delegate : GKSearchDelegate?
    public var keyword : String?{
        didSet{
            guard let keyword = keyword else { return }
            self.textField.text = keyword
            self.textAction(sender: self.textField)
        }
    }
    private var canTap : Bool?{
        didSet{
            guard let canTap = canTap else { return }
            self.searchBtn.isSelected = canTap
            self.searchBtn.isUserInteractionEnabled = canTap
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.delegate = self
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.cornerRadius = AppRadius * 2
        self.mainView.backgroundColor = UIColor(hex: "f6f6f6")
        self.textField.tintColor = Appx333333
        self.textField.textColor = Appx333333
        
        self.searchBtn.layer.masksToBounds = true
        self.searchBtn.layer.cornerRadius = AppRadius
        self.top.constant = STATUS_BAR_HIGHT
        
        
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:AppColor), for: .selected)
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:AppColor), for: UIControl.State(rawValue: UIControl.State.selected.rawValue | UIControl.State.highlighted.rawValue))
        
        
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:Appx999999), for: .normal)
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:Appx999999), for: UIControl.State(rawValue: UIControl.State.normal.rawValue | UIControl.State.highlighted.rawValue))
        
        self.searchBtn.isSelected = false
        self.textField.addTarget(self, action: #selector(textAction(sender:)), for: .editingChanged)
        
        self.searchBtn.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        self.backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    @objc func textAction(sender:UITextField){
        self.canTap = sender.text!.count > 0
    }
    @objc func searchAction(){
        if let delegate = self.delegate {
            delegate.searchTopView?(topView: self, keyword: self.textField.text!)
        }
    }
    @objc func backAction(){
        if let delegate = self.delegate{
            delegate.searchTopView?(topView: self, goback: true)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count == 0 {
            textField.resignFirstResponder();
            return false;
        }
        searchAction()
        return true;
    }
}

