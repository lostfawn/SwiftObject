//
//  GKClassifyTailCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKClassifyTailCell: UITableViewCell {

    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var subTitleLab: UILabel!
    
    @IBOutlet weak var countLab: UILabel!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var focusBtn: UIButton!
    
    @IBOutlet weak var nickNameBtn: UIButton!
    var model : GKBookModel?{
        didSet{
            guard let item = model else { return }

            self.imageV.setGkImageWithURL(url: item.cover  ?? "")
            self.titleLab.text = item.title ?? ""
            self.subTitleLab.attributedText  = attTitle(content: item.shortIntro ?? "")
            self.countLab.text = GKNumber.getCount(count: item.latelyFollower ?? 0)
            self.stateBtn.setTitle(item.majorCate ?? "", for: .normal)
            self.stateBtn.isHidden = item.majorCate?.count == 0 ? true : false
            self.focusBtn.setTitle("关注:"+String(item.retentionRatio)+("%"), for: .normal)
            self.nickNameBtn .setTitle(item.author ?? "", for: .normal)
            self.nickNameBtn.isHidden = item.author!.count > 0 ? false : true
        }
    }
     func attTitle(content :String) -> NSAttributedString {
        let paragraphStyle  = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .left
        paragraphStyle.allowsDefaultTighteningForTruncation = true
        let att = NSAttributedString.init(string:content.count > 0 ? content : "这家伙很懒,暂无简介!", attributes:[NSAttributedString.Key.paragraphStyle : paragraphStyle])
        return att
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.stateBtn.layer.masksToBounds = true
        self.stateBtn.layer.cornerRadius = 7.5
        self.focusBtn.layer.masksToBounds = true
        self.focusBtn.layer.cornerRadius = 10
        self.focusBtn.setTitleColor(AppColor, for: .normal)
        self.nickNameBtn.layer.masksToBounds = true
        self.nickNameBtn.layer.cornerRadius = AppRadius
        self.nickNameBtn.backgroundColor = Appxf8f8f8
        self.nickNameBtn.setTitleColor(Appx666666, for: .normal)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = AppRadius
        // Configure the view for the selected state
    }
    
    
}
