//
//  GKLaunchController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/18.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import Lottie

class GKLaunchController: BaseViewController {

    convenience init(completion:((() -> Void))? = nil) {
        self.init()
        self.completion = completion
    }
    private var completion :(() -> Void)? = nil
    private var timer: Timer!
    lazy var lottieView : AnimationView = {
        return AnimationView(name: "N")
    }()
    @IBOutlet weak var skipBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
    }
    @IBAction func skipAction(_ sender: UIButton) {
        self.stopTimer();
        self.dismissController();
    }
    private func loadUI(){
        self.fd_prefersNavigationBarHidden = true
        self.view.backgroundColor = UIColor.clear
        self.skipBtn.layer.masksToBounds = true
        self.skipBtn.layer.cornerRadius = 5
        self.skipBtn.layer.borderWidth = 1
        self.skipBtn.layer.borderColor = Appxf8f8f8.cgColor
        self.skipBtn.backgroundColor = Appx999999
        self.skipBtn.titleLabel?.font = UIFont.monospacedDigitSystemFont(ofSize: 14, weight: .regular)
        self.skipBtn.setTitleColor(UIColor.white, for: .normal)
        self.startTimer()
        self.view.addSubview(self.lottieView)
        self.lottieView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        self.lottieView.play { (finish) in
            
        }
        self.lottieView.loopMode = .loop
    }
    private func startTimer(){
        var time : Int = 3 - 1
        weak var mySelf = self
        self.skipBtn.setTitle(String(time) + "S跳过", for: .normal);
        self.timer = Timer.init(timeInterval: 1, repeats: true, block: { (timer) in
            if time < 1{
                mySelf!.skipAction(mySelf!.skipBtn)
                return
            }
            time = time - 1
            mySelf?.skipBtn.setTitle(String(time) + "S跳过", for: .normal)
        });
        RunLoop.current.add(self.timer, forMode: .common)
    }
    private func stopTimer(){
        if self.timer != nil {
            if self.timer.isValid {
                self.timer.invalidate()
            }
        }
    }
    private func dismissController(){
        if (self.completion != nil){
            self.completion!()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    override var prefersStatusBarHidden: Bool{
        return false
    }

}
