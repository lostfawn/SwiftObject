//
//  GKHomeMoreController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/10.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKHomeMoreController: BaseViewController {

    convenience init(info : GKHomeInfo) {
        self.init();
        self.bookInfo = info;
    }
    private var bookInfo:GKHomeInfo!
    private lazy var listData : [String] = {
        return []
    }()
    private lazy var rankData : [String] = {
        return []
    }()
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController();
        ctrl.magicView.separatorHeight = 0.5;
        ctrl.magicView.backgroundColor = Appxffffff
        ctrl.magicView.separatorColor = UIColor.clear;
        ctrl.magicView.navigationColor = Appxffffff;
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.sliderColor = AppColor
        ctrl.magicView.sliderExtension = 1;
        ctrl.magicView.bubbleRadius = 1;
        ctrl.magicView.sliderWidth = 15;
        
        ctrl.magicView.layoutStyle = .center;
        ctrl.magicView.navigationHeight = 30;
        ctrl.magicView.sliderHeight = 2;
        ctrl.magicView.itemSpacing = 20;
        
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = false;
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: self.bookInfo.shortTitle!)
        self.addChild(self.magicViewCtrl)
        self.view.addSubview(self.magicViewCtrl.view)
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        let model = self.bookInfo.rankModel
        if (model?.rankId.count)! > 0 {
            self.listData.append("周榜")
            self.rankData.append(model!.rankId)
        }
        if (model?.monthRank.count)! > 0 {
            self.listData.append("月榜")
            self.rankData.append(model!.monthRank)
        }
        if (model?.totalRank.count)! > 0 {
            self.listData.append("总榜")
            self.rankData.append(model!.totalRank)
        }
        self.magicViewCtrl.magicView.navigationHeight = self.listData.count > 1 ? 30 : 0.0
        self.magicViewCtrl.magicView.reloadData()
    }

}
extension GKHomeMoreController :VTMagicViewDataSource,VTMagicViewDelegate{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listData
    }
    func magicView(_ magicView: VTMagicView, menuItemAt itemIndex: UInt) -> UIButton {
        let button : UIButton = magicView.dequeueReusableItem(withIdentifier: "com.home.btn.itemIdentifier") ?? UIButton.init()
        button.setTitle(self.listData[Int(itemIndex)], for: .normal)
        button.setTitleColor(Appx333333, for: .normal)
        button.setTitleColor(Appx333333, for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize:14, weight: .regular)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        let vc = (magicView.dequeueReusablePage(withIdentifier: "com.home.ctrl.itemIdentifier")) ?? GKHomeItemController()
        let ctrl :GKHomeItemController = vc as! GKHomeItemController
        ctrl.rankId = self.rankData[Int(pageIndex)]
        return vc
    }
}
