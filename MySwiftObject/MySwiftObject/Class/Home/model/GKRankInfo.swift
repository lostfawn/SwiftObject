//
//  GKRankInfo.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON

class GKRankModel: HandyJSON {
    var rankId :String    = "";
    var monthRank:String = "";
    var totalRank:String = "";
    
    var collapse:String? = "";
    var cover:String? = "";
    var shortTitle:String? = "";
    var title:String? = "";
    var select:Bool? = false;

    func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.rankId <-- ["rankId","_id"]
    }
    required init() {}
    
}
class GKRankInfo: BaseModel {
    var male   :[GKRankModel]? = []
    var female :[GKRankModel]? = []
    var picture:[GKRankModel]? = []
    var epub   :[GKRankModel]? = []
    var state:Int? = 0;
    
    var boyDatas :[GKRankModel]?{
        get{
            let listData = userInfo.rankDatas
            if listData.count > 0 {
                listData.forEach { (objc) in
                    let list : [GKRankModel] = self.male?.filter({ (item) -> Bool in
                        item.rankId == objc.rankId
                    }) ?? []
                    if list.count > 0 {
                        let item : GKRankModel = list.first!
                        item.select = true
                    }
                }
            }
            return male
        }
    }
    var girlDatas :[GKRankModel]?{
        get{
            let listData = userInfo.rankDatas
            if listData.count > 0 {
                listData.forEach { (objc) in
                    let list : [GKRankModel] = self.female?.filter({ (item) -> Bool in
                        item.rankId == objc.rankId
                    }) ?? []
                    if list.count > 0 {
                        let item : GKRankModel = list.first!
                        item.select = true
                    }
                }
            }
            return female
        }
    }
}
