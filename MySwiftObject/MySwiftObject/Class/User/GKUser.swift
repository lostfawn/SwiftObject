//
//  GKUserModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON
import SwiftyJSON

public var userInfo  = GKUserManager.users()

private let GKUserInfo = "GKUserSex"
private let GKThemeInfo = "GKTheme"
public enum GKUserState : Int,HandyJSONEnum {
    case boy
    case girl
}
enum GKThemeState : Int,HandyJSONEnum {
    case Day
    case Night
}
public class GKUser: HandyJSON {
    var state     : GKUserState = .boy
    var rankDatas : [GKRankModel] = []
    required public init() {
    }
}
public class GKUserManager : NSObject{
    public class func saveUser(user :GKUser) {
        userInfo = user
        let defaults = UserDefaults.standard
        if let dic = user.toJSON(){
            defaults.set(dic, forKey: GKUserInfo)
            defaults.synchronize()
        }
    }
    public class func users() ->GKUser{
        let data = UserDefaults.standard.object(forKey:GKUserInfo)
        let json = JSON(data as Any)
        if let user : GKUser = GKUser.deserialize(from: json.rawString()){
            return user
        }
        return GKUser()
    }
}

class GKTheme : HandyJSON{
    var name  :String = "白天模式"
    var state :GKThemeState = .Day
    required public init() {
    }
}
class GKThemeTool :NSObject{
    static let manager = GKThemeTool()
    var theme :GKTheme?{
        get{
            return GKThemeTool.theme()
        }
    }
    class func setState(state :GKThemeState){
        let theme : GKTheme = GKThemeTool.manager.theme!
        if theme.state != state{
            theme.state = state
            GKThemeTool.saveTheme(theme: theme)
        }
    }
    class func saveTheme(theme :GKTheme){
        let defaults = UserDefaults.standard
        if let dic = theme.toJSON() {
            defaults.set(dic, forKey: GKThemeInfo)
            defaults.synchronize()
        }
    }
    class func theme() ->GKTheme{
        let data = UserDefaults.standard.object(forKey: GKThemeInfo)
        let json = JSON(data as Any)
        if let theme = GKTheme.deserialize(from: json.rawString()){
            return theme
        }
        return GKTheme()
    }
}
